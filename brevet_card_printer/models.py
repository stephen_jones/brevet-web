from django.db import models


# Create your models here.

class BrevetContainer:
    def __init__(self, name, distance, control_data):
        self.dict = {}
        self.dict['name'] = name
        self.dict['nominal_distance'] = int(distance)
        self.dict['controls'] = []
        for c in control_data:
            self.dict['controls'].append(
                {
                    'name': c[0],
                    'distance': float(c[1]),
                }
            )

    def __dict__(self):
        return self.dict

    @staticmethod
    def container_from_model(brevet_model):
        name = brevet_model.name
        distance = brevet_model.nominal_distance
        controldata = []
        for c_data in brevet_model.controldata_set.all():
            controldata.append((c_data.name, c_data.distance))
        return BrevetContainer(name, distance, controldata)


class BrevetData(models.Model):

    class Meta:
        verbose_name = 'Brevet'
        verbose_name_plural = 'Brevets'
        ordering = ['name']

    def __str__(self):
        return '{} {}'.format(self.name, self.nominal_distance)

    TWO = 200
    THREE = 300
    FOUR = 400
    SIX = 600
    THOUSAND = 1000
    TWELVE = 1200
    name = models.CharField(max_length=200)
    nominal_distance = models.IntegerField(
        choices=(
            (TWO, '200'),
            (THREE, '300'),
            (FOUR, '400'),
            (SIX, '600'),
            (THOUSAND, '1000'),
            (TWELVE, '1200'),
        ),
    )


class ControlData(models.Model):
    class Meta:
        verbose_name = 'Control'
        verbose_name_plural = 'Controls'
        ordering = ['distance']

    def __str__(self):
        return '{} {}'.format(self.name, self.distance)

    name = models.CharField(max_length=200)
    distance = models.DecimalField(default=0, max_digits=6, decimal_places=1)
    brevet = models.ForeignKey(BrevetData, on_delete='cascade')
