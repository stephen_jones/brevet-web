from selenium.webdriver.support.select import Select


class BrevetPage:
    def __init__(self, driver):
        self.driver=driver

    def brevet_name_input(self):
        return self.driver.find_element_by_id('brevet-name-input')

    def brevet_distance_select(self):
        return Select(self.driver.find_element_by_id('brevet-distance-select'))

    def control_name_input(self):
        return self.driver.find_element_by_id('control-name-input')

    def control_distance_input(self):
        return self.driver.find_element_by_id('control-distance-input')

    def add_control_button(self):
        return self.driver.find_element_by_id('add-control-button')

    def control_list(self):
        return self.driver.find_element_by_id('control-list')

    def get_all_controls(self):
        name_elements = self.control_list().find_elements_by_name('control-name')
        distance_elements=self.control_list().find_elements_by_name('control-distance')
        control_names = [c.get_property('value') for c in name_elements]
        distances = [c.get_property('value') for c in distance_elements]
        return list(zip(control_names, distances))

    def submit_button(self):
        return self.driver.find_element_by_id('submit-button')
