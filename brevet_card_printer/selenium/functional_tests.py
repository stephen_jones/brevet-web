import time
import unittest

from selenium import webdriver

from brevet_card_printer.selenium.brevet_page import BrevetPage
from brevet_card_printer.selenium.card_page import CardPage
from brevet_card_printer.selenium.home_page import HomePage


class PrintCardTest(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Chrome()
        self.addCleanup(self.driver.quit)
        self.home_page = HomePage(self.driver)
        self.card_page = CardPage(self.driver)
        self.brevet_page = BrevetPage(self.driver)

    def test_print_card(self):
        self.driver.get('http://localhost:8000')
        self.assertEqual('Brevet Control Card Creator', self.driver.title)
        self.assertEqual('Brevet Control Card Creator', self.home_page.header().text)
        # there is a list of brevets
        self.assertTrue(self.home_page.brevet_list().is_enabled())
        # clicking a brevet takes you to the brevet print page
        brevet_name = 'Three Lakes 1000'
        self.assertIn(brevet_name, self.home_page.brevet_list().text)
        self.home_page.get_brevet_link(brevet_name).click()
        time.sleep(2)
        self.assertEqual('Brevet Control Card Creator', self.driver.title)
        self.assertEqual(brevet_name, self.card_page.header().text)
        # can enter organizer contact info
        organizer_name = 'Anna Organizer'
        organizer_email = 'anna_o@rando.ca'
        organizer_phone = '416-555-1234'
        self.card_page.organizer_name_input().send_keys(organizer_name)
        self.card_page.organizer_email_input().send_keys(organizer_email)
        self.card_page.organizer_phone_input().send_keys(organizer_phone)
        self.assertEqual(
            organizer_name,
            self.card_page.organizer_name_input().get_property('value'))
        self.assertEqual(
            organizer_email,
            self.card_page.organizer_email_input().get_property('value'))
        self.assertEqual(
            organizer_phone,
            self.card_page.organizer_phone_input().get_property('value'))
        # can enter a bunch of names
        self.assertTrue(self.card_page.rider_name_input().is_enabled())
        rider_name1 = 'Joe Blow'
        self.card_page.rider_name_input().send_keys(rider_name1)
        self.card_page.add_rider_button().click()
        self.assertEqual(rider_name1, self.card_page.get_all_riders()[0])
        # can enter date
        # print button prints
        self.assertEqual(
            'Download Card File',
            self.card_page.print_button().text
            )
        self.card_page.print_button().click()

    def test_bare_card_request(self):
        self.driver.get('http://localhost:8000/card')

    def test_adhoc_card(self):
        # from home page, click ad-hoc
        self.driver.get('http://localhost:8000')
        self.home_page.ad_hoc_link().click()
        # enter brevet name and select distance
        self.brevet_page.brevet_name_input().send_keys('Test Brevet')
        self.brevet_page.brevet_distance_select().select_by_value('200')
        # enter controls and distances
        controls = [
            ('Start', '0'),
            ('First', '69'),
            ('Second', '136.4'),
            ('Finish', '206'),
        ]
        for control in controls:
            self.brevet_page.control_name_input().send_keys(control[0])
            self.brevet_page.control_distance_input().send_keys(control[1])
            self.brevet_page.add_control_button().click()
        # check controls entered
        actual_controls=self.brevet_page.get_all_controls()
        self.assertListEqual(controls,actual_controls)
        # next goes to brevet page
        self.brevet_page.submit_button().click()
        self.assertIn('ad-hoc-card', self.driver.current_url)


if __name__ == '__main__':
    unittest.main()
