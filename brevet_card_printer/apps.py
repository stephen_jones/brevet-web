from django.apps import AppConfig


class BrevetCardPrinterConfig(AppConfig):
    name = 'brevet_card_printer'
