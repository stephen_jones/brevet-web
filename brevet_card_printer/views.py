from datetime import datetime, timedelta

from django.http import HttpResponse
from django.shortcuts import (
    redirect,
    render,
)

import services
from services.card_maker import CardMaker
from .models import (
    BrevetContainer,
    BrevetData,
)


def home_page(request):
    brevets = BrevetData.objects.all()
    return render(request, 'home.html', {'brevets': brevets})


def card_page(request, brevet_id):
    brevet_model = BrevetData.objects.get(id=brevet_id)
    brevet = BrevetContainer.container_from_model(brevet_model)
    if request.method == 'POST':
        return do_pdf_creation(brevet.dict, request)
    return render(
        request,
        'card.html',
        {
            'brevet': brevet.dict,
            'def_start_time': get_next_saturday().isoformat(),
            }
        )


def do_pdf_creation(brevet, request):
    organizer = {}
    organizer['name'] = request.POST['organizer-name']
    organizer['email'] = request.POST['organizer-email']
    organizer['phone'] = request.POST['organizer-phone']
    rider_names = request.POST.getlist('rider-name')
    start_dtm = get_start_time(request)
    brevet_svc = services.Brevet(brevet, start_dtm)
    response = HttpResponse(content_type='application/pdf')
    CardMaker().print(response, brevet_svc, rider_names, organizer)
    return response


def ad_hoc_card(request):
    brevet = request.session['brevet']
    if request.method == 'POST':
        return do_pdf_creation(brevet, request)
    return render(
        request,
        'card.html',
        {
            'brevet': brevet,
            'def_start_time': get_next_saturday().isoformat(),
            }
        )


def bare_card(request):
    return render(request, 'bare_card.html')


def ad_hoc_brevet(request):
    if request.method == 'POST':
        control_names = request.POST.getlist('control-name')
        control_distance = request.POST.getlist('control-distance')
        brevet = BrevetContainer(
            name=request.POST['brevet-name'],
            distance=request.POST['brevet-distance'],
            control_data=zip(control_names, control_distance),
        )
        request.session['brevet'] = brevet.dict
        return redirect('ad-hoc-card')
    return render(request, 'brevet.html')


def get_start_time(request):
    return datetime.strptime(
        request.POST['date-time-result'],
        '%a, %b %d %Y %H:%M'
    )


def get_next_saturday():
    now = datetime.now()
    sat = now + timedelta((12 - now.weekday()) % 7)
    return sat.replace(hour=7, minute=0, second=0, microsecond=0)
