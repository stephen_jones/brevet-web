# Generated by Django 2.1 on 2018-08-12 16:50

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='BrevetData',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.IntegerField()),
                ('nominal_distance', models.CharField(choices=[(200, '200'), (300, '300'), (400, '400'), (600, '600'), (1000, '1000'), (1200, '1200')], max_length=4)),
            ],
        ),
        migrations.CreateModel(
            name='ControlData',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200)),
                ('distance', models.DecimalField(decimal_places=1, default=0, max_digits=6)),
                ('brevet', models.ForeignKey(on_delete='cascade', to='brevet_card_printer.BrevetData')),
            ],
        ),
    ]
