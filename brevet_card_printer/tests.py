import django
from django.test import TestCase
from django.urls import resolve

from brevet_card_printer.models import (
    BrevetData,
    ControlData,
    )
from brevet_card_printer.views import (
    ad_hoc_brevet,
    bare_card,
    home_page,
    )


class HomePageTest(TestCase):
    def test_root_url_resolves_to_home_page_view(self):
        found = resolve('/')
        self.assertEqual(home_page, found.func)

    def test_home_returns_correct_html(self):
        response = self.client.get('/')
        self.assertTemplateUsed(response, 'home.html')


class BrevetPageTest(TestCase):
    def setUp(self):
        self.brevet = BrevetData(name='A Brevet', nominal_distance='300')
        self.brevet.save()
        self.control1 = ControlData(
            name='Start',
            distance='0',
            brevet=self.brevet
        )
        self.control2 = ControlData(
            name='End',
            distance='310',
            brevet=self.brevet
        )
        self.control1.save()
        self.control2.save()

    def test_page_url_resolves_to_brevet_page_view(self):
        found = resolve('/card')
        self.assertEqual(bare_card, found.func)

    def test_page_url_returns_html(self):
        response = self.client.get('/card')
        self.assertTemplateUsed(response, 'bare_card.html')

    def test_brevet_page_returns_correct_html(self):
        response = self.client.get('/card/{}'.format(self.brevet.id))
        self.assertTemplateUsed(response, 'card.html')

    def test_brevet_page_POST(self):
        response = self.client.post(
            '/card/{}'.format(self.brevet.id),
            data={
                'organizer-name': 'Anne Organizer',
                'organizer-email': 'anne_o@rando.ca',
                'organizer-phone': '406-555-1234',
                'rider-name': 'Arnie Schwartz',
                'date-time-result': 'Sat, Sep 1 2018 07:00',
            }
        )
        self.assertEqual('application/pdf', response['Content-Type'])


class BrevetModelTest(TestCase):
    def test_saving_and_retrieving_brevets(self):
        brevet = BrevetData()
        brevet.name = 'Huron Shores'
        brevet.nominal_distance = '600'
        brevet.save()

        saved_items = BrevetData.objects.all()
        self.assertEqual(1, saved_items.count())
        self.assertEqual('Huron Shores', saved_items[0].name)
        self.assertEqual(600, saved_items[0].nominal_distance)

    def test_brevet_distance_constrained(self):
        brevet = BrevetData()
        brevet.name = 'Huron Shores'
        brevet.nominal_distance = '333'
        self.assertRaises(
            django.core.exceptions.ValidationError,
            brevet.full_clean,
        )

    def test_brevets_have_controls(self):
        brevet = BrevetData(name='Lakeshore East', nominal_distance='100')
        brevet.save()
        control = ControlData(brevet=brevet, name='Start', distance='0')
        control.save()
        saved_controls = ControlData.objects.filter(brevet=brevet)
        self.assertEqual(1, saved_controls.count())
        self.assertEqual('Start', saved_controls[0].name)
        self.assertEqual(0.0, saved_controls[0].distance)


class AdHocBrevetTest(TestCase):
    def test_page_url_resolves_to_view(self):
        found = resolve('/brevet')
        self.assertEqual(ad_hoc_brevet, found.func)

    def test_page_template_correct(self):
        response = self.client.get('/brevet')
        self.assertTemplateUsed(response, 'brevet.html')

    def test_brevet_page_POST(self):
        response = self.client.post(
            '/brevet',
            data={
                'brevet-name': 'Brevet Name',
                'brevet-distance': '200',
                'control-name': ['Start', 'First'],
                'control-distance': ['0', '67.8'],
            }
        )
