__author__ = 'Stephen'

from math import copysign
from datetime import timedelta


class TimeCalc(object):
    __fast_speed_to_200 = 34
    __fast_speed_to_400 = 32
    __fast_speed_to_600 = 30
    __fast_speed_to_1000 = 28
    __fast_speed_to_1300 = 26
    __slow_speed_to_600 = 15
    __slow_speed_to_1000 = 11.428
    __slow_speed_to_1300 = 13.333
    __seconds_in_minute = 60

    def __init__(self, start_time):
        self.__start_time = start_time

    def get_open_time(self, distance):
        """

        :param distance: distance in km from start
        :type distance: float
        :return: datetime
        :rtype: datetime.datetime
        """
        distance = self.__non_banker_round(distance)
        elapsed = (self.__open_first_200(distance)
                   + self.__open_200_to_400(distance)
                   + self.__open_400_to_600(distance)
                   + self.__open_600_to_1000(distance)
                   + self.__open_1000_to_1300(distance))
        elapsed = self.__round_to_minute(elapsed)
        return self.__start_time + elapsed

    def __open_first_200(self, distance):
        return self.__elapsed_time(distance, 0, 200, TimeCalc.__fast_speed_to_200)

    def __open_200_to_400(self, distance):
        return self.__elapsed_time(distance, 200, 400, TimeCalc.__fast_speed_to_400)

    def __open_400_to_600(self, distance):
        return self.__elapsed_time(distance, 400, 600, TimeCalc.__fast_speed_to_600)

    def __open_600_to_1000(self, distance):
        return self.__elapsed_time(distance, 600, 1000, TimeCalc.__fast_speed_to_1000)

    def __open_1000_to_1300(self, distance):
        return self.__elapsed_time(distance, 1000, 1300, TimeCalc.__fast_speed_to_1300)

    @staticmethod
    def __elapsed_time(distance, min_distance, max_distance, speed):
        if distance <= min_distance:
            return timedelta(0)
        if distance > max_distance:
            return timedelta(hours=(max_distance - min_distance) / speed)
        return timedelta(hours=(distance - min_distance) / speed)

    def get_close_time(self, distance):
        """

        :param distance: distance in km from start
        :type distance: float
        :return: datetime
        :rtype: datetime.datetime
        """
        distance = self.__non_banker_round(distance)
        elapsed = (self.__close_first_600(distance)
                   + self.__close_600_to_1000(distance)
                   + self.__close_1000_1300(distance))
        elapsed = self.__round_to_minute(elapsed)
        return self.__start_time + elapsed

    def __close_first_600(self, distance):
        if distance == 0:
            return timedelta(hours=1)
        else:
            return self.__elapsed_time(distance, 0, 600, TimeCalc.__slow_speed_to_600)

    def __close_600_to_1000(self, distance):
        return self.__elapsed_time(distance, 600, 1000, TimeCalc.__slow_speed_to_1000)

    def __close_1000_1300(self, distance):
        return self.__elapsed_time(distance, 1000, 1300, TimeCalc.__slow_speed_to_1300)

    @staticmethod
    def __round_to_minute(value_to_round):
        total_minutes = value_to_round.total_seconds() / TimeCalc.__seconds_in_minute
        rounded_minutes = round(total_minutes)
        return timedelta(minutes=rounded_minutes)

    def get_nominal_close(self, nominal_distance):
        if nominal_distance == 200:
            return self.__start_time + timedelta(hours=13, minutes=30)
        if nominal_distance == 300:
            return self.__start_time + timedelta(hours=20)
        if nominal_distance == 400:
            return self.__start_time + timedelta(hours=27)
        if nominal_distance == 600:
            return self.__start_time + timedelta(hours=40)
        if nominal_distance == 1000:
            return self.__start_time + timedelta(hours=75)
        if nominal_distance == 1200:
            return self.__start_time + timedelta(hours=90)

    @staticmethod
    def __non_banker_round(number):
        return int(number + copysign(0.5, number))
