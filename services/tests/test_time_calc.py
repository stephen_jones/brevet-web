import unittest
from datetime import (
    datetime,
    timedelta,
    )

from services.time_calc import TimeCalc


class TestTimeCalc(unittest.TestCase):
    def setUp(self):
        self.start_time = datetime(2000, 1, 1, 8)
        self.time_calc = TimeCalc(self.start_time)

    def test_get_opening_30(self):
        distance = 30
        expected = self.start_time + timedelta(minutes=53)
        actual = self.time_calc.get_open_time(distance)
        self.assertEqual(expected, actual)

    def test_get_closing_30(self):
        distance = 30
        expected = self.start_time + timedelta(minutes=120)
        actual = self.time_calc.get_close_time(distance)
        self.assertEqual(expected, actual)

    def test_get_open_zero(self):
        distance = 0
        expected = self.start_time
        actual = self.time_calc.get_open_time(distance)
        self.assertEqual(expected, actual)

    def test_get_close_zero(self):
        distance = 0
        expected = self.start_time + timedelta(hours=1)
        actual = self.time_calc.get_close_time(distance)
        self.assertEqual(expected, actual)

    def test_get_open_215(self):
        distance = 215
        expected = self.start_time + timedelta(hours=6, minutes=21)
        actual = self.time_calc.get_open_time(distance)
        self.assertEqual(expected, actual)

    def test_get_close_215(self):
        distance = 215
        expected = self.start_time + timedelta(hours=14, minutes=20)
        actual = self.time_calc.get_close_time(distance)
        self.assertEqual(expected, actual)

    def test_get_open_415(self):
        distance = 415
        expected = self.start_time + timedelta(hours=12, minutes=38)
        actual = self.time_calc.get_open_time(distance)
        self.assertEqual(expected, actual)

    def test_get_close_415(self):
        distance = 415
        expected = self.start_time + timedelta(days=1, hours=3, minutes=40)
        actual = self.time_calc.get_close_time(distance)
        self.assertEqual(expected, actual)

    def test_get_open_615(self):
        distance = 615
        expected = self.start_time + timedelta(hours=19, minutes=20)
        actual = self.time_calc.get_open_time(distance)
        self.assertEqual(expected, actual)

    def test_get_close_615(self):
        distance = 615
        expected = self.start_time + timedelta(days=1, hours=17, minutes=19)
        actual = self.time_calc.get_close_time(distance)
        self.assertEqual(expected, actual)

    def test_get_open_1015(self):
        distance = 1015
        expected = self.start_time + timedelta(days=1, hours=9, minutes=40)
        actual = self.time_calc.get_open_time(distance)
        self.assertEqual(expected, actual)

    def test_get_close_1015(self):
        distance = 1015
        expected = self.start_time + timedelta(days=3, hours=4, minutes=8)
        actual = self.time_calc.get_close_time(distance)
        self.assertEqual(expected, actual)

    def test_get_close_1200(self):
        distance = 1200
        expected = self.start_time + timedelta(hours=90)
        actual = self.time_calc.get_close_time(distance)
        self.assertEqual(expected, actual)

if __name__ == '__main__':
    unittest.main()
