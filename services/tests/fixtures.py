__author__ = 'Stephen'

from datetime import datetime
from brevet_card_printer.models import BrevetContainer
from services.brevet import Brevet


class Fixtures():
    @staticmethod
    def get_bewdley_one():
        return ("Tim Hortons", 0)

    @staticmethod
    def get_bewdley_two():
        return ("Kendal", 71.1)

    @staticmethod
    def get_bewdley_three():
        return ("Bewdley", 99.3)

    @staticmethod
    def get_bewdley_four():
        return ("Kendal", 127.6)

    @staticmethod
    def get_bewdley_five():
        return ("Tim Hortons", 205.3)

    def get_bewdley_brevet(self):
        start_time = datetime(2000, 1, 1)
        bewdley_container = BrevetContainer(
            "Bewdley Glutebuster",
            '200',
            [
                self.get_bewdley_one(),
                self.get_bewdley_two(),
                self.get_bewdley_three(),
                self.get_bewdley_four(),
                self.get_bewdley_five(),
            ]
        )
        bewdley = Brevet(bewdley_container.dict, start_time)
        return bewdley

    @staticmethod
    def get_lol_brevet():
        start_time = datetime(2000, 1, 1)
        controls = [
            ("Start", 0),
            ("US Customs", 145.2),
            ("Olcott", 198.3),
            ("Charlotte", 295.7),
            ("Webster", 321.6),
            ("Oswego", 430.6),
            ("Cape Vincent", 553.3),
            ("Canada", 597.5),
            ("Bath", 672.9),
            ("Brighton", 767.2),
            ("Bowmanville", 854.5),
            ("Stouffville", 916.4),
            ("Finish", 1012.4),
        ]
        lol_container = BrevetContainer("Lake Ontario Loop", 1000, controls)
        lol = Brevet(lol_container.dict, start_time)
        return lol

    @staticmethod
    def get_perfect_200():
        start_time = datetime(2000, 1, 1)
        container = BrevetContainer('200', 200, [('Finish', 200)])
        return Brevet(container.dict, start_time)

    @staticmethod
    def get_300_brevet():
        start_time = datetime(2000, 1, 1)
        container = BrevetContainer('', 300, [('Finish', 312.5)])
        brevet = Brevet(container.dict, start_time)
        return brevet

    @staticmethod
    def get_400_brevet():
        start_time = datetime(2000, 1, 1)
        container = BrevetContainer('', 400, [('Finish', 404.5)])
        brevet = Brevet(container.dict, start_time)
        return brevet

    @staticmethod
    def get_600_brevet():
        start_time = datetime(2000, 1, 1)
        brevet = Brevet(
            {
                'name': '',
                'nominal_distance': '600',
                'controls': [{'name': 'Finish', 'distance': '601.3'}]
            },
            start_time
        )
        return brevet

    @staticmethod
    def get_1200_brevet():
        start_time = datetime(2000, 1, 1)
        brevet = Brevet(
            {
                'name': '',
                'nominal_distance': '1200',
                'controls': [{'name': 'Finish', 'distance': '1210'}]
            },
            start_time
        )
        return brevet
