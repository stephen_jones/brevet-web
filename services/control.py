from datetime import datetime

from .equivalence import (
    DATE_FORMAT,
    Equivalence,
)


class Control(Equivalence):
    def __init__(self, control_name, control_distance):
        self.name = control_name
        try:
            self.distance = float(control_distance)
        except ValueError:
            self.distance = 0
        self.open_time = datetime(2000, 1, 1)
        self.close_time = datetime(2000, 1, 1)

    @property
    def open_time_text(self):
        return self.open_time.strftime(DATE_FORMAT)

    @property
    def close_time_text(self):
        return self.close_time.strftime(DATE_FORMAT)

    def __str__(self):
        return "{0} {1}".format(self.distance, self.name)
